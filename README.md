# nodal_analysis with broad conductors

Nodal analysis of GNR-Network for comparison with quantum transport approach.

In contrast to the usual nodal analysis approach, this model utilizes two-dimensional conductors. The tunneling conductance between to stripes is proportional to the amount of overlap.

## get started

Execute `main.py` and a calculation will take place. The results will be stored to the folder `\results`. From then on, feel free to experiment with different input parameters in `input.json`. 

Note that the use of a seed for the random number generation can be disabled by setting `"seed" : -1`. In that case, the system time will be used.

The tunneling conductance in the input file needs to be in G_0 / A, with G_0 being the conductance quantum and A being the area in square Angströms.

## useful input file parameters

### large, percolating network

```
{
    "length": 140.0,
    "width" : 60.0,
    "n_lines" : 30,
    "linelength" : 50.0,
    "linewidth" : 5.0,
    "seed" : 7,
    "is_plotting" : true,
    "outputdir" : "results/",
    "G_line" : 2.0,
    "G_tunnel" : 0.00009
}
```

### smaller, percolating network

```
{
    "length": 100.0,
    "width" : 100.0,
    "n_lines" : 20,
    "linelength" : 50.0,
    "linewidth" : 10.0,
    "seed" : 7,
    "is_plotting" : true,
    "outputdir" : "results/",
    "G_line" : 2.0,
    "G_tunnel" : 0.00009
}
```