'''
Main file to execute nodal analysis code.
'''

import json
import random
import time
import numpy as np

from modules.network_class import Network
from modules.line_class import Line

def main():
    time_start = time.time()

    print('NODAL ANALYSIS')


    print('\nreading input...')

    input_file = open('input.json')
    parameters = json.load(input_file)

    length = parameters['length']
    width = parameters['width']
    n_lines = parameters['n_lines']
    linelength = parameters['linelength']
    linewidth = parameters['linewidth']
    # layer_height = parameters['layer_height']
    seed = parameters['seed']
    is_plotting = parameters['is_plotting']
    outputdir = parameters['outputdir']

    G_line = parameters['G_line']
    G_tunnel = parameters['G_tunnel']

    random.seed(seed)

    print('\tG_line = {0} G0'.format(G_line))
    print('\tG_tunnel = {0} G0'.format(G_tunnel))



    print('creating percolating network...')

    n_tries_max = 1000000

    for i in range(n_tries_max):
        print('\ttry {0}'.format(i+1))
        print('\t- generating network...')

        nw = Network()

        nw.create_lines(
            length, width, 
            n_lines, 
            linelength, linewidth
            )

        nw.lines = sorted(
            nw.lines,
            key=lambda l: l.y
            )

        if is_plotting:
            nw.plot(outputdir + 'network_01.png', 'normal')




        print('\t- connecting to electrodes...')

        nw.check_electrode_connections()




        print('\t- finding clusters...')

        nw.find_clusters()
    
        if is_plotting:
            nw.plot(outputdir + 'network_02.png', 'clusters')


        is_percolating = nw.only_percolating()

        # is_percolating = True

        if is_percolating:
            tries_needed = i + 1
            print('\tpercolating network found after {0} tries!'.format(tries_needed))
            break
        else:
            print('\t-> not percolating :(')

    # if no percolating network was found
    else:
        print('\n\n=> ATTENTION: no percolating network found after {0} tries, programm terminated'.format(n_tries_max))
        return
    
        
    if is_plotting:
        nw.plot(outputdir + 'network_03.png', 'normal')



    print('determining vertices...')

    nw.determine_vertices()

    if is_plotting:
        nw.plot(outputdir + 'network_04.png', 'connections')


    print('calculating conductance...')

    nw.conductance_line = G_line
    nw.conductance_tunneling = G_tunnel

    G = nw.calculate_conductance(verbose=True)

    print('Done!')



    print('saving...')

    np.savetxt(outputdir + 'conductance.dat', [G])
    np.savetxt(outputdir + 'n_tries.dat', [tries_needed])

    # needed time

    time_end = time.time()
    time_needed = time_end - time_start

    print('\n\ntime needed: {0:.3f} s'.format(time_needed))

    np.savetxt(outputdir + 'time.dat', [time_needed])





if __name__ == '__main__':
    main()