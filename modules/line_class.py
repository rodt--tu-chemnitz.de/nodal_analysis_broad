import numpy as np
import math
import shapely
from shapely.geometry import Polygon as sPolygon

from modules.constants import LAYER_HEIGHT

class Line:
    # basics

    def __init__(self, r0=np.array([0.,0.]), dr=np.array([1.,0.]), width=1., y=.0, polygon=None, theta=0.):
        self.name = 'TBD'

        # self.r0 = np.array(r0)
        # self.dr = np.array(dr)
        
        ex = np.array([1., 0.])
        self.theta = np.arccos(np.dot(ex,dr) / np.linalg.norm(dr))
        if dr[0] < 0:
            self.theta *= -1

        self.y = y
        self.width = width

        if polygon is None:
            # polygons

            # unit vector perpendicular do dr
            dr_perp = np.array([dr[1], - dr[0]])
            dr_perp = dr_perp / np.linalg.norm(dr_perp)

            corners = [
                r0 - width * .5 * dr_perp,
                r0 - width * .5 * dr_perp + dr,
                r0 + width * .5 * dr_perp + dr,
                r0 + width * .5 * dr_perp
                ]
            
            self.polygon = sPolygon(corners)
        else:
            self.polygon = polygon

        # neighbors

        self.friends = []

        # if connecting to right or left side of box, making their vertices eligible
        # for being the electrodes

        # in nw.cut_to_size, it is checked if these apply

        self.connects_left = False
        self.connects_right = False

    # methods


    # calculating intersection of polygons

    def intersection_poly(self, line, use_y=None):
        
        # lines in same layer can intersect (for network generation)
        if use_y == 'layer':
            layerdist = abs( self.y - line.y )
            
            if layerdist > 10**(-3):
                return [False, None]
        # lines in adjacent layers can intersect       
        elif use_y == 'tunneling':
            layerdist = abs( self.y - line.y )

            if abs(layerdist - LAYER_HEIGHT) > 10**(-3):
                return [False, None]
        # if y-component doesn't matter, nothing needs to be done
        elif use_y is None:
            pass
        else:
            print('ERROR: invalid option of use_y={0}'.format(use_y))
            raise NotImplementedError

        # check for overlap

        polygon_1 = self.polygon
        polygon_2 = line.polygon

        is_intersecting = polygon_1.intersects(polygon_2)

        if is_intersecting:
            intersection = polygon_1.intersection(polygon_2)
            return [is_intersecting, intersection]
        else:
            return [False, None]
        
