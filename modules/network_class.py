import time
import random
import numpy as np
import shapely
from shapely.geometry import box as sbox
from shapely.geometry import Polygon as spolygon

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from modules.line_class import Line

import modules.toolbox as tb

# from constants import CONDUCTANCE_LINE
# from constants import CONDUCTANCE_TUNNELING
from modules.constants import CURRENT
from modules.constants import LAYER_HEIGHT

class Network:

    def __init__(self, G_line=.0, G_tunnel=.0):
        self.width = None
        self.width_vector = None
        self.length = None
        self.length_vector = None
        self.lines = []
        self.n_lines = None
        self.clusters = []
        self.basepolygon = None

        self.vertice_names = []
        self.vertice_intersections = []
        self.n_vertices = None

        self.conductance_line = G_line # G0
        self.conductance_tunneling = G_tunnel # G0


    ###########################################
    # Generation
    ###########################################

    def create_lines(self, length, width, n_lines, linelength, linewidth):

        self.length = length
        self.length_vector = np.array([self.length, 0.])
        self.width = width
        self.width_vector = np.array([0., self.width])
        self.n_lines = n_lines
        self.basepolygon = sbox(0., 0., self.length, self.width)

        zero_vector = np.array([0.,0.])

        for i in range(n_lines):
            # print('\ni = ', i)
            x0 = random.random() * length
            y0 = random.random() * width

            r0 = [x0, y0]
            dr = tb.randomDirection() * linelength


            linepolygon = Line(r0, dr, linewidth).polygon
            linepolygon_coords = linepolygon.exterior.coords

            # periodic boundary conditions
            x_min, y_min, x_max, y_max = linepolygon.bounds

            x_min_ok = 0. < x_min
            x_max_ok = x_max < self.length
            y_min_ok = 0. < y_min
            y_max_ok = y_max < self.width

            # print(x_min_ok, x_max_ok, y_min_ok, y_max_ok)

            sub_linepolygon_list = []
            shiftvector_list = [zero_vector]

            # 3-4 ok
            # x ok
            if x_min_ok and x_max_ok:
                # all good
                if y_min_ok and y_max_ok:
                    pass

                # too high
                elif y_min_ok and not y_max_ok:
                    shiftvector_list.append(-self.width_vector)

                # too low
                else:
                # elif not y_min_ok and y_max_ok:
                    shiftvector_list.append(+self.width_vector)

            # y ok
            elif y_min_ok and y_max_ok:
                # too much to the right
                if x_min_ok and not x_max_ok:
                    shiftvector_list.append(-self.length_vector)
                
                # too much to the left
                else:
                # elif not x_min_ok and x_max_ok:
                    shiftvector_list.append(+self.length_vector)
            
            # 2 ok -> corner placement

            # right side
            elif x_min_ok:
                shiftvector_list.append(-self.length_vector)

                # upper
                if y_min_ok:
                    shiftvector_list.append(-self.width_vector)
                    shiftvector_list.append(-self.width_vector-self.length_vector)
                
                # lower
                else:
                    shiftvector_list.append(+self.width_vector)
                    shiftvector_list.append(+self.width_vector-self.length_vector)
            
            # left side
            else:
                shiftvector_list.append(self.length_vector)

                # upper
                if y_min_ok:
                    shiftvector_list.append(-self.width_vector)
                    shiftvector_list.append(-self.width_vector+self.length_vector)
                
                # lower
                else:
                    shiftvector_list.append(+self.width_vector)
                    shiftvector_list.append(+self.width_vector+self.length_vector)


        

            for v in shiftvector_list:
                linepolygon
                coords = linepolygon_coords + v
                p = spolygon(coords)
                p = p.intersection(self.basepolygon)

                if not p.is_empty:
                    sub_linepolygon_list.append(p)
            
            subline_list = [Line(polygon=p) for p in sub_linepolygon_list]

            # get y-coordinate
            
            for sub_l in subline_list:
                while True:
                    # print(len(self.lines))
                    for line in self.lines:
                        # print(line.ame)
                        is_intersecting, intersection = sub_l.intersection_poly(line, use_y='layer')
                        # print('is_intersecting = ', is_intersecting)

                        if is_intersecting:
                            sub_l.y = sub_l.y + LAYER_HEIGHT
                            break
                    # if for-loop gets through withoug break, we break out of while-loop
                    else:
                        break

                self.lines.append(sub_l)
        
        self.n_lines = len(self.lines)

        for i in range(self.n_lines):
            self.lines[i].name = '{0}'.format(i)


    ###########################################
    # Manipulate
    ###########################################

    def check_electrode_connections(self):
        cutoff = 10**(-6)

        for l in self.lines:
            min_x, min_y, max_x, max_y = l.polygon.bounds

            # check left
            if min_x < cutoff:
                l.connects_left = True
            
            # check right
            if np.abs(max_x-self.length) < cutoff:
                l.connects_right = True


    def determine_vertices(self):
        for line_1 in self.lines:
            # sorting by pos along line so only neighboring vertices on a line will be connected
            M = tb.rotationMatrix(-line_1.theta)
            line_1.friends = sorted(
                line_1.friends, 
                key=lambda x: (np.mean(x['intersection'].exterior.coords, axis=0) @ M)[0]
                )

            for friend in line_1.friends:
                line_2 = friend['name']
                p = friend['intersection']

                vertice_name_now = [line_1, line_2]

                self.vertice_names.append(vertice_name_now)
                self.vertice_intersections.append(p)
        
        # total number of vertices
        self.n_vertices = len(self.vertice_names)

    ###########################################
    # Analyze
    ###########################################

    def find_intersections(self, use_y='tunneling'):
        '''
        Finds all intersections and returns coordinates of the
        center of the coordinates
        '''

        # print('\n\nFIND INTERSECTIONS')

        intersection_list = []

        for i in range(self.n_lines-1):
            line_1 = self.lines[i]
            for j in range(i+1, self.n_lines):
                line_2 = self.lines[j]
                
                intersection_exists, intersection = line_1.intersection_poly(line_2, use_y=use_y)

                if intersection_exists:
                    # print()
                    # leave out last point, as it gets counted twice otherwise
                    coords = intersection.exterior.coords[:-1]
                    # print(coords)
                    # averaging the corner coords to place coord in the middle
                    center = np.mean(coords, axis=0)
                    # print('center = ', center)
                    intersection_list.append(center)
        
        return intersection_list


    def find_clusters(self):
        '''
        constructs the connectivity-matrix out of the intersections,

        this function is still incomplete
        '''

        # finding neighbors

        for i in range(self.n_lines-1):
            line_1 = self.lines[i]
            for j in range(i+1, self.n_lines):
                line_2 = self.lines[j]

                intersection_exists, intersection = line_1.intersection_poly(line_2, use_y='tunneling')

                if intersection_exists:
                    # saving friends as dicts so we don't nee to calculate the
                    # coordinates again

                    friend_1 = {
                        'name' : line_2,
                        'intersection' : intersection
                        }
                    line_1.friends.append(friend_1)

                    friend_2 = {
                        'name' : line_1,
                        'intersection' : intersection
                        }
                    line_2.friends.append(friend_2)
        

        # get clusters

        clusters = []
        lines_used = []

        while True:
            lines_left = [l for l in self.lines if l not in lines_used]

            # if no lines are left to look for clusters, the search has ended
            if len(lines_left) == 0:
                break
            
            # connecting networks
            start_line = lines_left[0]

            lines_used.append(start_line)

            cluster_now = [start_line]
            
            old_lines = [start_line]
            new_lines = []

            while True:
                for line in old_lines:
                    for friend in line.friends:
                        linefriend = friend['name']
                        if linefriend not in cluster_now and linefriend not in new_lines and linefriend not in old_lines:
                            cluster_now.append(linefriend)
                            new_lines.append(linefriend)
                            lines_used.append(linefriend)

                if len(new_lines) == 0:
                    break

                old_lines = new_lines
                new_lines = []
            
            clusters.append(cluster_now)
        
        self.clusters = clusters

    
    def only_percolating(self):
        '''
        checks if clusters percolate and deletes all but the largest
        percolating cluster
        '''
        
        clusters_percolating = []

        for c in self.clusters:
            touch_left = False
            touch_right = False
            
            # look for left
            for l in c:
                l.connects_left
                if l.connects_left:
                    touch_left = True
                    break
            
            # look for right
            for l in c:
                if l.connects_right:
                    touch_right = True
                    break
            
            if touch_left and touch_right:
                clusters_percolating.append(c)
        
        if len(clusters_percolating) > 0:
            clusters_percolating = sorted(clusters_percolating, key=len)

            largest_cluster = clusters_percolating[-1]

            self.lines = largest_cluster
            self.clusters = [largest_cluster]
            self.n_lines = len(largest_cluster)

            return True
        else:
            return False


    def calculate_conductance(self, verbose=False):
        '''
        calculates the conductance of the newtork, assuming all lines are part of a 
        percolating network
        '''

        # constructing conductance matrix

        M = np.zeros((self.n_vertices, self.n_vertices))

        # connecting vertices
        for i in range(self.n_vertices-1):
            vertice_1 = self.vertice_names[i]
            # print('\ni = {0}, v_1 = {1}'.format(i, vertice_1))

            for j in range(i+1, self.n_vertices):
                vertice_2 = self.vertice_names[j]
                # print('-> j = {0}, v_2 = {1}'.format(j, vertice_2))

                # connecting lines
                if i+1 == j and vertice_1[0].name == vertice_2[0].name:
                    # print('\t\tLINE')
                    M[i,j] = - self.conductance_line
                    M[j,i] = - self.conductance_line
                # connecting tunneling contacts
                elif vertice_1[0].name == vertice_2[1].name and vertice_1[1].name == vertice_2[0].name:
                    # print('\t\tTUNNEL')
                    intersection_now = self.vertice_intersections[i]
                    area = intersection_now.area
                    
                    conductance_tunneling_now = self.conductance_tunneling * area

                    M[i,j] = - conductance_tunneling_now
                    M[j,i] = - conductance_tunneling_now
        
        # assigning main diagonal elements

        for i in range(self.n_vertices):
            column_sum = np.sum(M[:,i])
            M[i,i] = - column_sum

        # determining witch vertices have a current coming in/out
        # we pick the left-/rightmost vertices connected to the electrodes 

        index_left = None
        px_left = self.length

        index_right = None
        px_right = .0

        for v, intersec, i in zip(self.vertice_names, self.vertice_intersections, range(self.n_vertices)):
            line = v[0]

            x_center = np.mean(intersec.exterior.coords, axis=0)[0]
            
            if line.connects_left:
                if index_left is None or x_center < px_left:
                    index_left = i
                    px_left = x_center

            if line.connects_right:
                if index_right is None or x_center > px_right:
                    index_right = i
                    px_right = x_center


        current_vector = np.zeros((self.n_vertices,))
        current_vector[index_left] = CURRENT # in to the left
        current_vector[index_right] = -CURRENT # out to the right

        # earthing
        # we set the potential to zero at the vertice connected with the right electrode

        mode = 'delete'

        # print(index_left, ' ', index_right)

        possible_earthings = list(range(self.n_vertices))

        possible_earthings.remove(index_left)
        possible_earthings.remove(index_right)

        earthing_index = random.choice(possible_earthings)

        # define use indizes to decide what values to grab later
        # we need these to later take into account that the indizes change under certain earthings
        if mode == 'delete':
            if earthing_index < index_left:
                index_left_use = index_left - 1
            else:
                index_left_use = index_left
            
            if earthing_index < index_right:
                index_right_use = index_right - 1
            else:
                index_right_use = index_right

            # delete row
            M = np.delete(M, earthing_index, axis=0)
            # delete column
            M = np.delete(M, earthing_index, axis=1)
            # delete row in current vector
            current_vector = np.delete(current_vector, earthing_index, axis=0)
        elif mode == 'set':
            for i in range(self.n_vertices):
                M[i, earthing_index] = M[earthing_index, i] = .0
            M[earthing_index, earthing_index] = 1.
        else:
            print('ERROR: mode {0} not implemented'.format(mode))
            raise NotImplementedError


        # checking numerics
        if verbose:
            condition = np.linalg.cond(M)
            print('\tmatrix condition: {0:.2e}'.format(condition), end=' ')
            if condition < 10**15:
                print('(good)')
            else:
                print('(bad)')


        # solving for conductance

        M_inv = np.linalg.inv(M)

        potentials = M_inv @ current_vector


        if earthing_index == index_right and mode == 'delete':
            voltage = potentials[index_left_use]
        elif earthing_index == index_left and mode == 'delete':
            voltage = - potentials[index_right_use]
        else:
            voltage = potentials[index_left_use] - potentials[index_right_use]

        if verbose:
            print('\tvoltage: {0} V'.format(voltage))

        # calculate conductance
        # U = I * R <=> U * G = I <=> G = I / U

        G = CURRENT / voltage

        if verbose:
            print('\tconductance: {0} G0'.format(G))

        return G

    ###########################################
    # Visualization
    ###########################################
    
    def plot(self, filename, mode, border='tight'):
        fig, ax = plt.subplots()

        colors = ['C{0}'.format(i) for i in range(30)]


        if mode == 'normal' or mode == 'connections':
            y_list = [l.y for l in self.lines]

            y_min = np.min(y_list)
            y_max = np.max(y_list)
            y_delta = y_max - y_min

            colormap = plt.get_cmap('autumn')

            # all z-coords are the same
            if np.abs(y_delta) < 10**(-6):
                color = colormap(0.)

                for l in self.lines:
                    p = mpatches.Polygon(
                        xy=l.polygon.exterior.coords,
                        facecolor=color,
                        zorder=10,
                        edgecolor='black'
                        )
                    ax.add_patch(p)
            else: 

                for l in self.lines:
                    colorindex = (l.y - y_min) / y_delta * 1.
                    color = colormap(colorindex)

                    p = mpatches.Polygon(
                        xy=l.polygon.exterior.coords,
                        facecolor=color,
                        zorder=l.y+1,
                        edgecolor='black'
                        )
                    ax.add_patch(p)

        elif mode == 'clusters':
            # plotting lines
            for c, color in zip(self.clusters, colors):
                for l in c:
                    p = mpatches.Polygon(
                        xy=l.polygon.exterior.coords,
                        facecolor=color,
                        edgecolor='black'
                        )
                    ax.add_patch(p)
        else:
            print('ERROR: mode {0} not implemented'.format(mode))
            raise NotImplementedError

        # plotting intersections

        if mode == 'connections':
            vertice_positions = [np.mean(x.exterior.coords[:-1], axis=0) for x in self.vertice_intersections]
            vertice_positions = np.array(vertice_positions)

            # draw lines

            for i in range(self.n_vertices-1):
                vertice_1 = self.vertice_names[i]
                vertice_1_pos = vertice_positions[i]
                
                for j in range(i+1, self.n_vertices):
                    vertice_2 = self.vertice_names[j]
                    vertice_2_pos = vertice_positions[j]

                    if i+1 == j and vertice_1[0].name == vertice_2[0].name:
                        xy = np.transpose([vertice_1_pos, vertice_2_pos])
                        
                        ax.plot(
                            *xy,
                            zorder=99,
                            color='black',
                            linewidth=3.
                            )
            
            # draw nodes

            ax.plot(
                *vertice_positions.T, 
                linestyle=' ', 
                marker='o',
                markeredgecolor='black', 
                color='white',
                zorder=100,
                )
            
            current_in = min(
                vertice_positions,
                key = lambda x: x[0]
                )
            current_out = max(
                vertice_positions,
                key = lambda x: x[0]
                )
            
            current_nodes = [current_in, current_out]

            for xy in current_nodes:
                ax.plot(
                    *xy, 
                    linestyle=' ', 
                    marker='o',
                    markeredgecolor='black', 
                    color='tab:blue',
                    zorder=101,
                    markersize=10
                    )



        else:
            intersections = np.transpose(self.find_intersections())

            ax.plot(*intersections, linestyle=' ', marker='o', markeredgecolor='black', color='white',zorder=100)

        # background rectangle

        area_rectangle = mpatches.Rectangle(
            (0,0), self.length, self.width,
            color='lightgray',
            zorder=0
            )

        ax.add_patch(area_rectangle)


        # electrodes
        if False:
            y_list = [.0, self.width]
            for x in [.0, self.length]:
                x_list = [x, x]
                ax.plot(
                    x_list, 
                    y_list, 
                    color='gray', 
                    zorder=1,
                    linewidth=4.,
                    linestyle='-'
                    )
        
        # borders and axes labels

        if border == 'tight':
            border = np.max([self.width, self.length]) * .00
            ax.set_xlim(-border, self.length + border)
            ax.set_ylim(-border, self.width + border)
        else:
            pass

        ax.set_aspect('equal')
        ax.tick_params(
            direction='inout',
            which='both',
            top=True,
            right=True,
            zorder=200
            )
        ax.set_axisbelow(False)
        
        ax.set_xlabel(r'$x$ [\AA]')
        ax.set_ylabel(r'$y$ [\AA]')

        # saving

        fig.savefig(filename, dpi=300, bbox_inches='tight')
        plt.close()
