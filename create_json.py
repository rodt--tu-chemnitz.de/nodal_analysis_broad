'''
Author: Tom Rodemund
Date: 08.09.2020

This file creates the input json file for the main programm
and accepts arguments via the command zone.
'''

import json
import sys
import time

def main():
    print('CREATING JSON')
    '''
    sys.argv arguments:

    0... script name
    1... network length
    2... network width
    3... n_lines
    4... linelength
    5... linewidth
    6... seed
    7... is_plotting
    8... outputdir
    9... G_line
    10... G_tunnel
    '''
    
    # reading the command line arguments

    parameters = {}

    parameters['length'] = float(sys.argv[1])
    
    parameters['width'] = float(sys.argv[2])

    parameters['n_lines'] = int(sys.argv[3])

    parameters['linelength'] = float(sys.argv[4])

    parameters['linewidth'] = float(sys.argv[5])

    parameters['seed'] = float(sys.argv[6])
    if parameters['seed'] < 0:
        parameters['seed'] = time.time()

    parameters['is_plotting'] = bool(int(sys.argv[7]))

    parameters['outputdir'] = str(sys.argv[8])

    parameters['G_line'] = float(sys.argv[9])
    parameters['G_tunnel'] = float(sys.argv[10])

    for key in parameters.keys():
        print('{0} = {1}'.format(key, parameters[key]))


    # saving as json
    
    with open('input.json', 'w') as outfile:
        json.dump(parameters, outfile)


if __name__ == '__main__':
    main()